import com.studinskyi.readfiles_inthreads.FileCounterReader;
import com.studinskyi.readfiles_inthreads.PoolCounters;
import com.studinskyi.readfiles_inthreads.ProjectUtilits;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.fail;

public class PoolCountersTest {
    private String workingDirectory;
    private String testingDirectory;
    private String nameTestingFile;
    private File testingFile;
    private Date startTestsTime;

    @Before
    public void setUp() throws Exception {
        System.out.println("Before run tests " + ProjectUtilits.getCurrentDateTimeString());
        workingDirectory = "c:\\test_QA\\";
        startTestsTime = new Date();
        // String fileSeparator = File.separator;
        //System.out.println("Current directory for reding files: " + workingDirectory);

        String fileSeparator = File.separator;
        String nameTestDirectory = "test_FileRead_" + ProjectUtilits.getCurrentDateTimeToNameFile() + (int) (Math.random() * 100);
        testingDirectory = workingDirectory + nameTestDirectory + fileSeparator;
        System.out.println("Current directory for reding files: " + testingDirectory);

        // создание временного каталога и тестовых файлов для подсчета чисел при последующем их прочтении
        testingDirectory = ProjectUtilits.setWorkFolder(testingDirectory);
    }

    @Test
    public void checkPoolCounterReaderTest() {
        // создание тестовых файлов
        for (int i = 1; i <= 10; i++) {
            // запист в новый текстовый файл блока текста с числами, определенного в самом методе записи файла createTestFile
            createTestFile(testingDirectory, "text" + i, "");
        }

        // открытие потока для получение списка путей к файлам из указанной папки
        // и запуск чтения файлов в отдельных потоках
        PoolCounters poolCounters = new PoolCounters(testingDirectory);
        poolCounters.runAndSummingNumbersFromTreads();
        System.out.println("Total sum of positive numbers =  " + poolCounters.getSumNumbersOfPool());

        // сверка полученного результа с известной суммой из тестовой выборки 10-ти файлов, равная 700
        int totalSumOfThreads = poolCounters.getSumNumbersOfPool();
        Assert.assertEquals(700, totalSumOfThreads);
    }

    @Test
    public void checkMethod_readResource() {
        // создание ОДНОГО тестового файла
        nameTestingFile = createTestFile(testingDirectory, "text_1_readResource", "");

        PoolCounters poolCounters = new PoolCounters(testingDirectory);
        // запуск потоков через ExecutorService чтобы ограничить одновременную работу до 2 потоков
        ExecutorService serviceExecutor = Executors.newFixedThreadPool(2);
        // запуск чтения и подсчета ОДНОГО файла в отдельном объекте потока Thread
        poolCounters.readResource(new File(testingDirectory + nameTestingFile), 1, serviceExecutor);

        serviceExecutor.shutdown();
        while (!serviceExecutor.isTerminated()) {
            System.out.println("ожидание завершения ExecutorService " + ProjectUtilits.getCurrentDateTimeString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                fail(e.getMessage());
                e.printStackTrace();
            }
        }

        // сверка полученного результа с известной суммой чисел из тестового файла, равной 70
        int totalSumOfThread = poolCounters.getSumNumbersOfPool();
        Assert.assertEquals(70, totalSumOfThread);
    }

    @After
    public void tearDown() throws Exception {
        // удаление тестовых файлов после их прочтения
        List<File> listFiles = ProjectUtilits.listFilesInDirectory(testingDirectory);
        for (File elemFile : listFiles)
            deleteFile(testingDirectory, elemFile.getName());

        // удаление временного каталога, использованного для выполнения данного локального теста
        deleteFile(testingDirectory, "\\"); // удалить пустой каталог можно тем же методом что и файл

        // обнуление счетчика общей суммы в классе FileCounterReader
        FileCounterReader.setCountPositiveNumbers(0);

        System.out.println("After all tests have been completed " + ProjectUtilits.getCurrentDateTimeString());
    }

    public String createTestFile(String folderFile, String nameFile, String textToFile) {
        String fullPathToFile = "";
        String lineSeparator = System.getProperty("line.separator");

        //получаем текущую дату и время
        Date newTime = new Date();
        long msDelay = newTime.getTime() - startTestsTime.getTime(); //вычисляем число наносекунд, прошедших со старта теста

        // задание гарантированно уникального имени файла
        nameFile = nameFile + "test_" + ProjectUtilits.getCurrentDateTimeToNameFile() + "_" + msDelay + ".txt";

        if (textToFile.equals(""))
            textToFile = "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1" + lineSeparator +
                    "2 4  6  15 -34 9 19 -8 2" + lineSeparator +
                    "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1" + lineSeparator +
                    "2 4  6  15 -34 9 19 -8 2" + lineSeparator +
                    "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1";

        if (!nameFile.equals("")) {
            fullPathToFile = folderFile + nameFile; // + File.separator
            if (!ProjectUtilits.fileExist(fullPathToFile)) {
                //if (!f.exists()) {
                try {
                    OutputStream outStream = new FileOutputStream(fullPathToFile);
                    outStream.write(textToFile.getBytes());
                    outStream.close();

                    if (ProjectUtilits.fileExist(fullPathToFile))
                        System.out.println("file was created: " + fullPathToFile);
                } catch (IOException e) {
                    System.out.println("did not created file: " + fullPathToFile);
                }
            } else {
                System.out.println("file is already exists: " + fullPathToFile);
            }
        }
        return nameFile;
    }

    public void deleteFile(String folderFile, String nameFile) {
        String fullPathToFile = "";

        if (!nameFile.equals("")) {
            fullPathToFile = folderFile + nameFile; // + File.separator
            if (ProjectUtilits.fileExist(fullPathToFile)) {
                //if (f.exists()) {
                //System.out.println("file is deleted: " + fullPathToFile);
                File f = new File(fullPathToFile);
                //                    file.setReadOnly();//mark this file as read only, since jdk 1.2
                //                    if(file.canWrite()){
                //                        System.out.println("This file is writable");
                //                    }else{
                //                        System.out.println("This file is read only");
                //                    }
                //                    file.setWritable(true);//revert the operation, mark this file as writable, since jdk 1.6
                //                    if(file.canWrite()){
                //                        System.out.println("This file is writable");
                //                    }else{
                //                        System.out.println("This file is read only");
                //                    }
                if (!f.canWrite()) {
                    System.out.println("This file is read only: " + fullPathToFile);
                    //                    try {
                    //                        throw new ExceptionFileIsReadOnly("This file is read only: " + fullPathToFile);
                    //                    } catch (ExceptionFileIsReadOnly e) {
                    //                        e.printStackTrace();
                    //                    }
                }

                // непосредственно команда удаления файла
                f.delete();

                if (ProjectUtilits.fileExist(fullPathToFile)) {
                    System.out.println("File has not been deleted:" + fullPathToFile);
                    //                    try {
                    //                        throw new ExceptionFileNotDeleted("File has not been deleted:" + fullPathToFile);
                    //                    } catch (ExceptionFileNotDeleted e) {
                    //                        e.printStackTrace();
                    //                    }
                }

            } else {
                System.out.println("file does not exist, it can not be deleted: " + fullPathToFile);
            }
        }
    }

}