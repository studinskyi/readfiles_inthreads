import com.studinskyi.readfiles_inthreads.FileCounterReader;
import com.studinskyi.readfiles_inthreads.ProjectUtilits;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.fail;

public class FileCounterReaderTest {

    private String workingDirectory;
    private String testingDirectory;
    private String nameTestingFile;
    private Date startTestsTime;

    @Before
    public void setUp() throws Exception {
        System.out.println("Before run tests " + ProjectUtilits.getCurrentDateTimeString());
        workingDirectory = "c:\\test_QA\\";
        startTestsTime = new Date();
        // String fileSeparator = File.separator;
        //System.out.println("Current directory for reding files: " + workingDirectory);

    }

    @Test
    public void checkFileCounterReaderTest() {
        String fileSeparator = File.separator;
        String nameTestDirectory = "test_FileRead_" + ProjectUtilits.getCurrentDateTimeToNameFile() + (int) (Math.random() * 100);
        testingDirectory = workingDirectory + nameTestDirectory + fileSeparator;
        System.out.println("Current directory for reding files: " + testingDirectory);

        // создание временного каталога и тестовых файлов для подсчета чисел при последующем их прочтении
        testingDirectory = ProjectUtilits.setWorkFolder(testingDirectory);

        // создание ОДНОГО тестового файла
        nameTestingFile = createTestFile(testingDirectory, "text_1_checkFileCounter", "");

        FileCounterReader counterReader = new FileCounterReader(testingDirectory + nameTestingFile);
        Thread thread = new Thread(counterReader, "thread_1_checkFileCounter_");
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            fail(e.getMessage());
            e.printStackTrace();
        }
        int totalSumInFile = FileCounterReader.getCountPositiveNumbers();
        Assert.assertEquals(70, totalSumInFile);
        //Assert.assertEquals(70, totalSumInFile);

        // удаление тестовых файлов после их прочтения
        List<File> listFiles = ProjectUtilits.listFilesInDirectory(testingDirectory);
        for (File elemFile : listFiles)
            deleteFile(testingDirectory, elemFile.getName());

        // удаление временного каталога, использованного для выполнения данного локального теста
        deleteFile(testingDirectory, "\\"); // удалить пустой каталог можно тем же методом что и файл

        // обнуление счетчика общей суммы в классе FileCounterReader
        FileCounterReader.setCountPositiveNumbers(0);
    }


    @Test
    public void checkMethod_countNumbersFromFile() {
        String fileSeparator = File.separator;
        String nameTestDirectory = "test_FileRead_" + ProjectUtilits.getCurrentDateTimeToNameFile() + (int) (Math.random() * 100);
        testingDirectory = workingDirectory + nameTestDirectory + fileSeparator;
        System.out.println("Current directory for reding files: " + testingDirectory);

        // создание временного каталога и тестовых файлов для подсчета чисел при последующем их прочтении
        testingDirectory = ProjectUtilits.setWorkFolder(testingDirectory);

        // создание ОДНОГО тестового файла
        nameTestingFile = createTestFile(testingDirectory, "text_1_checkFileCounter", "");

        FileCounterReader counterReader = new FileCounterReader(testingDirectory + nameTestingFile);
        int totalSumInMethod = 0;
        try {
            totalSumInMethod = counterReader.countNumbersFromFile();
        } catch (IOException e) {
            fail(e.getMessage());
            e.printStackTrace();
        }
        int totalSumInFile = FileCounterReader.getCountPositiveNumbers();
        //        assertEquals(totalSumInMethod, totalSumInFile);
        //        assertEquals(70, totalSumInFile);
        Assert.assertEquals(70, totalSumInFile);
        //Assert.assertEquals(totalSumInMethod, totalSumInFile);
        //Assert.assertEquals(70, totalSumInFile);

        // удаление тестовых файлов после их прочтения
        List<File> listFiles = ProjectUtilits.listFilesInDirectory(testingDirectory);
        for (File elemFile : listFiles)
            deleteFile(testingDirectory, elemFile.getName());

        // удаление временного каталога, использованного для выполнения данного локального теста
        deleteFile(testingDirectory, "\\"); // удалить пустой каталог можно тем же методом что и файл

        // обнуление счетчика общей суммы в классе FileCounterReader
        FileCounterReader.setCountPositiveNumbers(0);
    }

    @After
    public void tearDown() throws Exception {


        System.out.println("After all tests have been completed " + ProjectUtilits.getCurrentDateTimeString());
    }

    public String createTestFile(String folderFile, String nameFile, String textToFile) {
        String fullPathToFile = "";
        String lineSeparator = System.getProperty("line.separator");

        //получаем текущую дату и время
        Date newTime = new Date();
        long msDelay = newTime.getTime() - startTestsTime.getTime(); //вычисляем число наносекунд, прошедших со старта теста

        // задание гарантированно уникального имени файла
        nameFile = nameFile + "test_" + ProjectUtilits.getCurrentDateTimeToNameFile() + "_" + msDelay + ".txt";

        if (textToFile.equals(""))
            textToFile = "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1" + lineSeparator +
                    "2 4  6  15 -34 9 19 -8 2" + lineSeparator +
                    "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1" + lineSeparator +
                    "2 4  6  15 -34 9 19 -8 2" + lineSeparator +
                    "3 5 7 -10 -20 2  5  -4  41" + lineSeparator +
                    "23   -5  4  8  1";

        if (!nameFile.equals("")) {
            fullPathToFile = folderFile + nameFile; // + File.separator
            if (!ProjectUtilits.fileExist(fullPathToFile)) {
                //if (!f.exists()) {
                try {
                    OutputStream outStream = new FileOutputStream(fullPathToFile);
                    outStream.write(textToFile.getBytes());
                    outStream.close();

                    if (ProjectUtilits.fileExist(fullPathToFile))
                        System.out.println("file was created: " + fullPathToFile);
                } catch (IOException e) {
                    System.out.println("did not created file: " + fullPathToFile);
                }
            } else {
                System.out.println("file is already exists: " + fullPathToFile);
            }
        }
        return nameFile;
    }

    public void deleteFile(String folderFile, String nameFile) {
        String fullPathToFile = "";

        if (!nameFile.equals("")) {
            fullPathToFile = folderFile + nameFile; // + File.separator
            if (ProjectUtilits.fileExist(fullPathToFile)) {
                //if (f.exists()) {
                File f = new File(fullPathToFile);
                if (!f.canWrite()) {
                    System.out.println("This file is read only: " + fullPathToFile);
                }

                // непосредственно команда удаления файла
                f.delete();

                if (ProjectUtilits.fileExist(fullPathToFile)) {
                    System.out.println("File has not been deleted:" + fullPathToFile);
                }

            } else {
                System.out.println("file does not exist, it can not be deleted: " + fullPathToFile);
            }
        }
    }

}